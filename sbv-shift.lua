#!/usr/bin/env lua
local arg = arg or {...}

assert(#arg>1, "You must provide options and filename(s) of .sbv files to process!")

function printerr(...)
	io.stderr(table.concat({...}, "\t"), "\r\n")
end

function shortTime2Time(str, name)
	local sign = str:match("^%-") and -1 or 1
	local sec,ms = str:match("[+%-]?(%d*)%.?(%d*)")
	if ms and #ms < 3 then
		ms = ms .. string.rep("0", 3 - #ms)
	end
	sec,ms = tonumber(sec) or 0,tonumber(ms) or 0
	
	assert(not (sec == 0 and ms == 0), (name or "value ") .. " of 0.0 is not valid!")
	return sign * (sec * 1000 + ms)
end


for i = 1, #arg do
	if arg[i] == "-h" or arg[i] == "--help" then
		printerr([[sbv-shift.lua - .sbv subtitle editor
Modifies all subtitles in a .sbv by specified options.
sbv-shift.lua [OPTIONS/FILES...]
-h | --help - show this help text
-s | --shift <time> - shift subtitles by <time>
-a | --start <time> - add/substract <time> from subtitle starting time
-z | --end <time> - add/substract <time> from subtitle ending time
<time> is a required argument, supports only "sec.ms" format: -12.42 / 1.337]])
		os.exit(0)
	end
end

-- [-s <time> | --shift=<time>]
local time_shift = 0
for i = 1, #arg do
	-- epic pattern
	local str = arg[i]:match("%-%-?sh?i?f?t?[ =](%S+)")
	if str then
		time_shift = shortTime2Time(str, "Timeshift")
	end
end

local start_move = 0
for i = 1, #arg do
	-- epic pattern
	local str = arg[i]:match("%-%-?[as]t?a?r?t?[ =](%S+)")
	if str then
		start_move = shortTime2Time(str, "Starttime-move")
	end
end

local end_move = 0
for i = 1, #arg do
	-- epic pattern
	local str = arg[i]:match("%-%-?[ze]n?d?[ =](%S+)")
	if str then
		end_move = shortTime2Time(str, "Endtime-move")
	end
end

--print(time_shift,start_move,end_move)

function updateTimestamp(str)
	local h1,m1,s1,ms1, h2,m2,s2,ms2 = str:match("(%d+):(%d+):(%d+)%.(%d+),(%d+):(%d+):(%d+)%.(%d+)")
	local t1 = h1*3600*1000 + m1*60*1000 + s1*1000 + ms1 + time_shift + start_move
	local t2 = h2*3600*1000 + m2*60*1000 + s2*1000 + ms2 + time_shift + end_move
	--print(h1,m1,s1,ms1, h2,m2,s2,ms2)
	local new_ts = string.format("%01d:%02d:%02d.%03d,%01d:%02d:%02d.%03d",
		math.floor(t1/3600000), math.floor(t1/60000) % 60, math.floor(t1/1000) % 60, t1 % 1000,
		math.floor(t2/3600000), math.floor(t2/60000) % 60, math.floor(t2/1000) % 60, t2 % 1000
	)
	--print(new_ts)
	return assert(new_ts)
end

for i = 1, #arg do
	if arg[i]:sub(1,1) ~= "-" then
		local file = assert(io.open(arg[i], "rb"))
		local text = file:read("*a")
		file:close()
		--file = assert(io.open(arg[i] ..".shifted", "wb"))
		text = text:gsub("(%d+:%d+:%d+%.%d+,%d+:%d+:%d+%.%d+)", updateTimestamp)
		io.stdout:write(text)
		--file:close()
	end
end
