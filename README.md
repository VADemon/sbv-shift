## .sbv subtitle editor - shifter & modifier
Allows to edit subtitles in bulk, of one whole file: shift, expand or shrink subtitles of the .sbv (Youtube) subtitle format.

### Usage
```
sbv-shift.lua [OPTIONS/FILES...]
-h | --help - show this help text
-s | --shift <time> - shift subtitles by <time>
-a | --start <time> - add/substract <time> from subtitle starting time
-z | --end <time> - add/substract <time> from subtitle ending time
<time> is a required argument, supports only "sec.ms" format: -12.42 / 1.337
```

**Linux:** `lua sbv-shift.lua` or `./sbv-shift.lua` (with correct +x permission)

**Windows:** `sbv-shift.lua` or `lua sbv-shift.lua`

### Installation
**Requirements: Lua5.1+**

**Debian/Ubuntu:** `sudo apt install lua`

**Windows:** [LuaForWindows](https://github.com/rjpcomputing/luaforwindows/releases) or any other Lua distribution

### Example:
* Shift subtitles by 500ms forward: `lua sbv-shift.lua --shift 0.5 file.sbv > file-shifted.sbv`
* Shift subtitles by -325ms and expand end time by 100ms: `lua sbv-shift.lua --shift -0.325 --end 100 file.sbv > file-shifted.sbv`